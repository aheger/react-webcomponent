import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Header from './header';

export default () => (
	<Router>
		<Header />
	</Router>
);
