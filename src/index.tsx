import * as React from 'react';
import * as ReactDOM from 'react-dom';

import App from './app';

class HeaderWebComponent extends HTMLElement {
	constructor() {
		super();
	}

	connectedCallback() {
		ReactDOM.render(<App />, this);
	}

	disconnectedCallback() {
		ReactDOM.unmountComponentAtNode(this);
	}
}

customElements.define('my-header', HeaderWebComponent);
