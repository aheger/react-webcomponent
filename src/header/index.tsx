import * as React from 'react';
import { NavLink } from 'react-router-dom';

import './index.scss';

export default () => {
	return <header>
		<ul className="header-links">
			<li className="header-link">
				<NavLink to="/" exact={true} activeClassName='is-active'>Home</NavLink>
			</li>
			<li className="header-link">
				<NavLink to="/products" exact={true} activeClassName='is-active'>Products</NavLink>
			</li>
			<li className="header-link">
				<NavLink to="/blog" exact={true} activeClassName='is-active'>Blog</NavLink>
			</li>
		</ul>
	</header>;
};
